<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenjualanDetail extends Model
{
    use HasFactory;

    protected $table = 'penjualan_details'; 
    public $fillable = [
        'penjualan_id',
        'product_id',
        'qty'
    ];

    public function penjualan()
    {
        return $this->belongsTo(Penjualan::class, 'penjualan_id');
    }

    public function book()
	{
		return $this->belongsTo('App\Models\Book', 'book_id');
	}
}
