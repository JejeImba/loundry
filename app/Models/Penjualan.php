<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    use HasFactory;

    protected $table = 'penjualans'; 
    public $fillable = [
        'id',
        'no_po',
        'user_id',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'user_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'penjualan_details', 'penjualan_id', 'product_id')
		->withPivot('id','qty')
		->withTimestamps();
    }
}
