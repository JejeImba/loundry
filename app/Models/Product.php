<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products'; 
    public $fillable = [
        'id',
        'name',
        'price',
        'qty',
        'status'
    ];

    public function penjualans()
    {
        return $this->belongsToMany(Penjualan::class, 'penjualan_details', 'penjualan_id', 'product_id')
        ->withPivot('qty')
        ->timestamps();
    }
}
