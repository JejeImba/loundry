<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class UserController extends Controller
{
    public function index()
	{
		$users = User::all();
		return view('users.index',compact('users'));
	}

	public function create()
	{
		return view('users.create');
	}

	public function store(Request $request)
	{
		$request->validate([
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'no_telp' => 'required|string|min:5|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'status' => $request->status,
            'address' => $request->address,
            'no_telp' => $request->no_telp,
            'password' => Hash::make($request->password),
        ]);
        event(new Registered($user));
		return redirect()->route('user.index')->with('User berhasil di tambahkan');
	}

    public function edit($id)
	{
		$user = User::find($id);
		return view('users.edit',compact('user'));
	}

    public function update(Request $request, $id)
	{
		$user = User::find($id)->update($request->all());
		return redirect()->route('user.index')->with('success','Data has Updated');
	}
}
