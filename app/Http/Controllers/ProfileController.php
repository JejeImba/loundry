<?php

namespace App\Http\Controllers;
use App\Http\Controllers\ProfileController;
use App\Models\Profiles;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
	{
		$user_profiles = User::all();
		return view('profiles.index',compact('user_profiles'));
	}

    public function show(Request $request, $id)
	{
		if(Auth::id() == $id) {
            // valid user
            $user_profiles = Auth::user();
            return view('profiles.show', compact("user_profiles"));
       } else {
            //not allowed
            echo 'not allowed';
       }
	}
}
