<?php

namespace App\Http\Controllers;
use App\Models\Penjualan;
use App\Models\PenjualanDetail;
use App\Models\Product;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class PenjualanController extends Controller
{
    public function index()
	{
		$sales = Penjualan::all();
		return view('penjualans.index',compact('sales'));
	}

	public function create() 
	{
		//supaya pilihan users dan produk muncul di halaman penjualan
		$users = DB::table('users')->where('status', '=', 'user')->get();
		$products = Product::all();
		return view('penjualans.create', compact('users', 'products'));
	}

	public function store(Request $request) 
	{
		$request->validate([
			'user_id' => 'required',
			'no_po' => 'required|unique:penjualans',
			'product_id' => 'required',
			'qty' => 'required',
		]);
		//multiple insert penjualan
		$sales = new Penjualan;
		$sales->user_id = $request->user_id;
		$sales->no_po = $request->no_po;
		$sales->save();
		$id = $sales->id;
		if($id != 0) {
			foreach($request->product_id as $key => $v) {
				$data = array(
					'penjualan_id' => $id,
					'product_id' => $v,
					'qty' => $request->qty[$key],
				);
				PenjualanDetail::insert($data);
			}
		}
		return redirect()->route('dashboard')->with('success','penjualan berhasil dibuat no Order ' . '<b>'. $sales->id .'</b>');
	}
}
