<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'Pewangi Seterika',
                'price' => '20000',
                'qty' => '5',
                'status' => 'jualan',
            ],
            [
                'name' => 'Detergen',
                'price' => '25000',
                'qty' => '10',
                'status' => 'jualan',
            ],
            [
                'name' => 'Cuci Jeans',
                'price' => '7000',
                'qty' => NULL,
                'status' => 'cucian',
            ]
        ]);
    }
}
