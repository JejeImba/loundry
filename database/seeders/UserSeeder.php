<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'J J Mohamad Yusuf',
                'email' => 'jeje.yusuf@yahoo.com',
                'address' => 'perumahan sakinah batam center no 16',
                'no_telp' => '081295805494',
                'password' => Hash::make('121314'),
                'status' => 'admin',
            ],
            [
                'name' => 'Pak Abu',
                'email' => 'gg.gg@gg.com',
                'address' => 'Komplek Perumahan Belum Jadi Blok C4',
                'no_telp' => '081255803494',
                'password' => Hash::make('121314'),
                'status' => 'user',
            ],
            [
                'name' => 'Pak Yana',
                'email' => 'jeje.jeje@jeje.com',
                'address' => 'Komplek Perumahan Sudah diJual',
                'no_telp' => '081295804494',
                'password' => Hash::make('121314'),
                'status' => 'user',
            ],
            [
                'name' => 'Armen',
                'email' => 'imba.jeje@yahoo.com',
                'address' => 'Komplek Perumahan Sudah Jadi Blok B2',
                'no_telp' => '081285123494',
                'password' => Hash::make('121314'),
                'status' => 'kasir',
            ],

        ]);
    }
}
