<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User Management') }}
        </h2>
    </x-slot>

    <div class="col-md-11 col-md-offset-1">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
    @if (Auth::user()->status === 'admin' || 'kasir')
    <a class="btn btn-success btn-sm" href="{{ route('user.create') }}">Create Manual User</a>

    <table class="table table-striped">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Status</th>
            <th>Email</th>
            <th>Alamat</th>
            <th width="210">Action</th>
        </tr>
        @foreach ($users as $key => $user)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->status}} </td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->address }}</td>
                <td> 
                    <a class="btn btn-primary btn-sm" href="{{ route('user.edit',$user->id) }}">Edit</a>
                </td>
            </tr>
        @endforeach
    </table>
    

    @else
    Page not found 404 | <a href="{{ route('dashboard') }}">Back</a>
@endif
</x-app-layout>

