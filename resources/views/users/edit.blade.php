<x-app-layout>
@if (Auth::user()->status === 'admin')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit User Profile') }}
        </h2>
    </x-slot>
    {!! Form::model($user, ['method' => 'PATCH','route' => ['user.update', $user->id]]) !!}
        <div class="col-md-6 col-xs-6 konten">
            <div class="form-group">
                <strong>Nama User : </strong>
                {!! Form::text('name', null, array('placeholder' => 'Nama','class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                <strong>Alamat : </strong>
                {!! Form::text('address', null, array('placeholder' => 'Alamat','class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                <strong>Nomor Telpon : </strong>
                {!! Form::text('no_telp', null, array('placeholder' => 'Nomor Telpon','class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                <strong>Email : </strong>
                {!! Form::email('email', null, array('placeholder' => 'Nomor Telpon','class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                <strong>Status User: </strong>
                {!! Form::select('status', ['user' => 'User', 'kasir' => 'Kasir'], 'S', ['class' => 'form-control' ]) !!}
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {!! Form::close() !!}
    @endif
</x-app-layout>




