<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Main Profile') }}
        </h2>
    </x-slot>

    You're logged in! 
                    @if (Auth::user()->status === 'admin')
                        Welcome Admin <b>{{Auth::user()->name}}</b>
                    @elseif (Auth::user()->status === 'kasir')
                        Welcome Kasir <b>{{Auth::user()->name}}</b>
                    @else
                        Welcome Customer <b>{{Auth::user()->name}}</b> 
                    @endif 
</x-app-layout>

