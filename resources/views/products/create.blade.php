@if (Auth::user()->status === 'admin' && 'kasir')
    <x-app-layout>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Buat Pesanan') }}
            </h2>
        </x-slot>    
        {!! Form::open(array('route' => 'product.store','method'=>'POST')) !!}

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="col-xs-5 col-sm-5 col-md-5">
            <div class="form-group">
                <strong>Nama Product : </strong>
                {!! Form::text('name', null, array('placeholder' => 'Nama','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-5 col-sm-5 col-md-5">
            <div class="form-group">
                <strong>Harga : </strong>
                {!! Form::text('price', null, array('placeholder' => 'Harga','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-5 col-sm-5 col-md-5">
            <div class="form-group">
                <strong>Jumlah : </strong>
                {!! Form::text('qty', null, array('placeholder' => 'Jumlah','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-5 col-sm-5 col-md-5">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

{!! Form::close() !!}     
    </x-app-layout>   
@else
Error 404! Page tidak tersedia <a href="{{ route('dashboard') }}">kembali</a>
@endif 
