<x-app-layout>
@if (Auth::user()->status === 'admin')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Product') }}
        </h2>
    </x-slot>
    {!! Form::model($product, ['method' => 'PATCH','route' => ['product.update', $product->id]]) !!}
        <div class="col-md-6 col-xs-6 konten">
            <div class="form-group">
                <strong>Nama Produk : </strong>
                {!! Form::text('name', null, array('placeholder' => 'Nama','class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                <strong>Harga : </strong>
                {!! Form::text('price', null, array('placeholder' => 'Harga','class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                <strong>Status Produk : </strong>
                {!! Form::select('status', ['active' => 'Aktif', 'disactive' => 'Tidak Aktif'], 'S', ['class' => 'form-control' ]) !!}
            </div>

            <div class="form-group">
                <strong>Jumlah : </strong>
                {!! Form::text('qty', null, array('placeholder' => 'Jumlah', 'class' => 'form-control','readonly')) !!}
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    {!! Form::close() !!}
    @endif
</x-app-layout>




