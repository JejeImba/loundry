<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Product') }}
        </h2>
    </x-slot>

    <div class="col-md-11 col-md-offset-1">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
    @if (Auth::user()->status === 'admin')
    <a class="btn btn-success btn-sm" href="{{ route('product.create') }}">Create New Item</a>

    <table class="table table-striped">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Jumlah</th>
            <th>Harga/Perbotol/Kilo</th>
            <th width="210">Action</th>
        </tr>
        @foreach ($products as $key => $product)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $product->name }}</td>
                <td> @if ($product->qty < 1)
                        - 
                    @else 
                        {{$product->qty}}
                    @endif </td>
                <td>{{ $product->price }}</td>
                <td> 
                    <a class="btn btn-primary btn-sm" href="{{ route('product.edit',$product->id) }}">Edit</a>
                </td>
            </tr>
        @endforeach
    </table>
    

    @else
    <table class="table table-striped">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Jumlah</th>
            <th>Harga/Perbotol/Kilo</th>
        </tr>
        @foreach ($products as $key => $product)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $product->name }}</td>
                <td> @if ($product->qty < 1)
                        - 
                    @else 
                        {{$product->qty}}
                    @endif </td>
                <td>{{ $product->price }}</td>
            </tr>
        @endforeach
    </table>
@endif
</x-app-layout>

