
<x-app-layout>
@if (Auth::user()->status === 'admin' || 'kasir')
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Buat Pesanan') }}
        </h2>
    </x-slot> 

    {{-- pesan error --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' => 'penjualan.store','method'=>'POST')) !!}
        @csrf

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-group">
                    <strong>Nomor PO : </strong>
                    {!! Form::text('no_po', null, array('placeholder' => 'Nomor PO','class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-group">
                    <strong>Customer : </strong>
                    <select name="user_id" id="user_id" class="form-control">
                        <option selected="true" disabled="true" value="0">Nama Customer</option>
                            @foreach($users as $user => $b)        
                        <option value="{{ $b->id }}" @if (old("product_id")){{ (in_array($b->id, old("product_id")) ? "selected":"") }} @endif>{{$b->name}}</option>
                            @endforeach
                    </select>
                </div>
            </div>

            <label># Penjualan Detail </label>
            <a href="javascript:void(0)" onclick="AddPenjualan()" title="Tambahkan Item" class="btn pull-right btn-sm btn-success"><i class="fa fa-plus"></i> Add</a>  
            <table class="table table-hover" id="TData">
                <thead>
                    <tr>
                        <th>Book Title</th>                            
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>

        <script type="text/javascript">        
            function AddPenjualan()
            {
                var row = 
                '<tr>'+
                    '<td>'+
                        '<select name="product_id[]" class="form-control" id="product_id">'+
                            '<option selected="true" disabled="true" value="0">Select Item</option>'+
                                '@foreach($products as $product => $b)' +       
                            '<option value="{{ $b->id }}" @if (old("product_id")){{ (in_array($b->id, old("product_id")) ? "selected":"") }} @endif>{{$b->name}}</option>'+
                                '@endforeach'+
                        '</select>'+
                    '</td>'+
                    '<td>'+
                        '<input type="text"class="form-control" required name="qty[]">'+
                    '</td>'+
                    '<td>'+
                        '<div></div>'+
                    '</td>'+
                    '<td>'+
                        '<a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"></i> Delete</a>'+
                    '</td>'
                    
                '</tr>';

                $("#tbody").append(row);
                
            };
            function deleteRow(btn)
            {
                var row = btn.parentNode.parentNode;
                row.parentNode.removeChild(row);
            };
            AddPenjualan(); 
        </script>

        <script>
            
        </script>
        <div class="col-xs-5 col-sm-5 col-md-5">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    {!! Form::close() !!}   
    @else
        Error 404! Page tidak tersedia <a href="{{ route('dashboard') }}">kembali</a>
    @endif       
</x-app-layout>   



