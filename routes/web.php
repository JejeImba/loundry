<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

// route index profiles
Route::group(['middleware' => ['auth']], function()
{
      Route::group(['prefix' => 'profiles'], function()
      {
            Route::get('/', 'App\Http\Controllers\ProfileController@index')->name('profile.index');
            Route::get('/create','ProfileController@create')->name('profile.create');
            Route::post('/store','ProfileController@store')->name('profile.store');
            Route::get('/{profiles}/edit', 'ProfileController@edit')->name('profile.edit');
            Route::PATCH('/{profiles}/update', 'ProfileController@update')->name('profile.update');
      });
});

Route::group(['middleware' => ['auth']], function()
{
      Route::group(['prefix' => 'penjualans'], function()
      {
            Route::get('/', 'App\Http\Controllers\PenjualanController@index')->name('penjualan.index');
            Route::get('/create','App\Http\Controllers\PenjualanController@create')->name('penjualan.create');
            Route::post('/store','App\Http\Controllers\PenjualanController@store')->name('penjualan.store');
            Route::get('/search', 'PenjualanController@search')->name('penjualan.search');
            Route::get('/{penjualans}/edit', 'PenjualanController@edit')->name('penjualan.edit');
            Route::PATCH('/{penjualans}/update', 'PenjualanController@update')->name('penjualan.update');
            Route::get('/{penjualans}/show', 'PenjualanController@show')->name('penjualan.show');
      });
});

Route::group(['middleware' => ['auth']], function()
{
      Route::group(['prefix' => 'products'], function()
      {
            Route::get('/', 'App\Http\Controllers\ProductController@index')->name('product.index');
            Route::get('/create','App\Http\Controllers\ProductController@create')->name('product.create');
            Route::post('/store','App\Http\Controllers\ProductController@store')->name('product.store');
            Route::get('/{products}/edit', 'App\Http\Controllers\ProductController@edit')->name('product.edit');
            Route::PATCH('/{products}/update', 'App\Http\Controllers\ProductController@update')->name('product.update');
            Route::get('/{products}/show', 'ProductController@show')->name('product.show');
      });
});

Route::group(['middleware' => ['auth']], function()
{
      Route::group(['prefix' => 'users'], function()
      {
            Route::get('/', 'App\Http\Controllers\UserController@index')->name('user.index');
            Route::get('/create','App\Http\Controllers\UserController@create')->name('user.create');
            Route::post('/store','App\Http\Controllers\UserController@store')->name('user.store');
            Route::get('/{users}/edit', 'App\Http\Controllers\UserController@edit')->name('user.edit');
            Route::PATCH('/{users}/update', 'App\Http\Controllers\UserController@update')->name('user.update');
            Route::get('/{users}/show', 'UserController@show')->name('user.show');
      });
});

//route show profiles
// Route::get('/profiles', function () {
//     return view('profiles.show');
// })->middleware(['auth'])->name('profile.show');

require __DIR__.'/auth.php';
